class Solution(object):
    def groupAnagrams(self, strs):
        """
        -Create a dictionary, key is a length 26 tuple of letter frequencies, value is an array of strings with those letter frequencies
        """
        outputDict = {}
        for s in strs:
            frequencies = [0]*26
            for c in s:
                frequencies[ord(c)-97] += 1
            tFrequencies = tuple(frequencies)
            if tFrequencies in outputDict.keys():
                outputDict[tFrequencies].append(s)
            else:
                outputDict[tFrequencies] = [s]
        return list(outputDict.values())